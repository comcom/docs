# Palvelun käyttöönotto

Keskustelualusta lisätään verkkosivulle laittamalla HTML-koodipohjaan linkit sekä Javascript-, että CSS-tyylitiedostoomme.

## Asennus

1. Lisää seuraava rivi sivun HTML-koodin `<head>` tagin sisälle:

```html
<head>

...

<!-- Lisää alla oleva rivi HTML-koodipohjaan -->
<link href="https://assets.comcom.dev/Widget.css" rel="stylesheet" />

</head>
```

2. Lisää viite Javascript-skriptitiedostoon `<body>` tagin loppuun, eli juuri ennen sulkevaa `</body>` tagia:

```html
<body>

...
<!-- Lisää alla oleva rivi HTML-koodipohjaan -->
<script async src="https://assets.comcom.dev/Widget.js"></script>
</body>
```

### Kielen valinta (valinnainen)

Palvelu näkyy kaikille käyttäjille englannin kielisenä, ellei sivuston ylläpitäjä konfiguroi kieltä itse.
Kieli asetetaan lisäämällä seuraava `<meta />` HTML-tag lähdekoodiin `<head />` tagin sisään:

```html
<head>
<!-- Lisää alla oleva rivi HTML-koodipohjaan -->
<meta name="comcom-language" content="fi" />

...

</head>
```

Tämä metatagi lisäämällä palvelu näkyy nyt kaikille sivuston käyttäjille suomen kielellä.

### Infosivu

Jos palvelun ylläpitäjä haluaa näyttää käyttäjälle kanavapalkissa näkyvästä (?)-infopainikkeesta avautuvan infosivuston, niin alla oleva metatietotagi määrittelee infosivun osoitteen:

```html
<meta name="comcom-service-info" content="http://www.comcom.chat/tietosuoja" />
```

Esimerkiksi ylläoleva johtaa Comcomin omalle tietosuojasivulle.

## Toiminta

Näiden lisäyksien ansiosta lukijan selain lataa sovelluksemme ja käynnistää sen seuraavasti:

* Teknisesti sovellus yrittää ladattuaan ottaa Websocket-yhteyden palvelimiimme.
* Jos yhteys saadaan muodostettua onnistuneesti, ilmestyy sininen Comcomin Cc-logoikoni näytön oikeaan alakulmaan.
* Jos yhteyttä ei saada muodostettua, käyttäjän selain ei näytä Cc-logoa tai mitään muutakaan erikoista isäntäsivulla.
* Kun Websocket-yhteys saadaan muodostettua, se pidetään aktiivisena käyttäjän koko sivun selaussession ajan.
* Punainen pallo logon päällä ilmaisee, että käyttäjällä on uusia lukemattomia viestejä keskustelualustalla.

Aktiivisena pidettävästä Websocket-yhteydestä on tiettyjä hyötyjä sivuston ylläpitäjille. Esimerkiksi Ylläpitäjä näkee täysin reaaliajassa sivustoa selaavien käyttäjien määrän, sekä myös tällä hetkellä aktiivisten rekisteröityneiden käyttäjien määrän.

Kun alusta halutaan poistaa käytöstä sivulla, poistetaan yllä olevat lisäykset HTML-koodipohjaan. Tämän jälkeen käyttäjän selain ei enää lataa ja käynnistä Comcom-keskustelualustaa sivuston avatessaan.
