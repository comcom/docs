# Palveun hallinta
> Ohjeet palvelun moderaattoreille

## Käyttäjähierarkia

Kanavalla on viisiasteinen käyttäjähierarkia, jossa käyttäjille sallitut oikeudet määräytyvät statuksen (ns. _privilege_) mukaan. 

Palvelussa on viisi eri käyttäjätyyppiä seuraavilla oikeuksilla:

- **Passiiviset käyttäjät**: eivät ole rekisteröityneet palveluun. Näkevät keskustelun mutta eivät pysty osallistumaan siihen. Lukijan avatessa verkkosivun tämä on palvelun perusnäkymä
- **Normaalit käyttäjät**: rekisteröityneet palveluun, mutta ei erityisiä oikeuksia. Eivät pysty lähettämään viestejä kun kanava mykistettu (mute)
- **VIP-käyttäjät**: pystyvät lähettämään viestejä silloin kun kanava on mykistetty. Ei muita erityisoikeuksia
- **ADMIN-käyttäjät**: Moderointioikeudet. Pystyvät tekemään kaiken muun kanavalla paitsi nimeämään lisää moderoijia.
- **OWNER-käyttäjät**: Korkein statusluokka. Kaikki moderointioikeudet, sekä pystyvät lisäksi nimittämään muita käyttäjiä moderoijiksi ja viemään moderointioikeudet.

## Kanavan hallinta

Moderointikomentojen ohjeet tulevat esille moderointioikeudet omaaville käyttäjille (`ADMIN`, `OWNER`) kirjoittamalla merkin / tekstikenttään.

Lisäksi moderaattorit näkevät reaaliajassa:

- rekisteröityneiden käyttäjien määrän
- passiivisten käyttäjien määrän

Kumpikin luku näkyy kanavapalkissa kanavan nimen vieressä. Suluissa oleva luku on passiivisten käyttäjien (eli rekisteröitymättömien käyttäjien yhteyksien) määrän.

Kyseiset luvut kertovat juuri tällä hetkellä verkkosivulla olevien käyttäjien määrän, ei ylipäätään joskus rekisteröityneiden käyttäjien määrän.

### Mykistystila

Moderoija voi laittaa kanavalle päälle ns. mykistystilan. Kanavan ollessa mykistettynä ainoastaan normaaleja käyttäjiä korkeammalla statuksella olevat käyttäjät pystyvät lähettämään viestejä.

Mykistystila asetetaan päälle seuraavalla moderointikomennolla:
```
/mute
```
ja vastaavasti poistetaan komennolla
```
/unmute
```

Mykistystila on kätevä keino esimerkiksi hiljentää kanava yöksi, kun moderoija ei ole paikalla. Esimerkiksi tilaajille tai muille hyvin käyttäytyville käyttäjille voi antaa VIP-statuksen, jotta he voivat jatkaa keskustelua moderoijan poistuttua.

### Otsikko

Kanavalle voi asettaa otsikon. Otsikko on jokaiselle käyttäjälle ensimmäisenä näkyvä viesti, kun käyttäjä avaa palvelun. Otsikossa voi ilmoittaa esimerkiksi kanavan säännöt, viimeisimmät uutiset sekä moderoijien nimimerkit.

Otsikko asetetaan `/topic`-komennolla seuraavasti:
```
/topic Tervetuloa kanavalle ksml.fi! Nyt: Veneet miltei törmäsi päijänteellä - keskustelu #päijänne alla.
```

Kun kanavalta halutaan poistaa otsikko, asetetaan `/topic` ilman mitään otsikkoa, eli yksinkertaisesti:

```
/topic
```

### Ylentäminen ja alentaminen

Käyttäjiä voidaan ylentää ja alentaa kanavalla antamalla hänelle eri oikeudet. `ADMIN`-oikeudet tulee antaa pelkästään luotetuille henkilöille, sillä moderointioikeuksilla pystyy aiheuttamaan tuhoa kanavalla. Hyvä ajatus voi olla esimerkiksi ylentää luotetut käyttäjät tai lehden tilaajat `VIP`-käyttäjiksi. Tällöin he pystyvät jatkamaan keskustelua myös silloin, jos kanava esimerkiksi mykistetään yöksi muilta käyttäjiltä.

Oikeuksien asettaminen tapahtuu seuraavalla komennolla:

```
/assign @nimimerkki oikeus
```

Nimimerkki on oltava etumerkillä `@`.
Oikeuden on oltava joku seuraavista: `admin`, `vip`, `none`.
`none` tarkoittaa käyttäjän oikeuksien poistamista, eli käyttäjästä tulee tällöin normaali käyttäjä.

`owner`-käyttäjiä ei moderaattorit pysty asettamaan.

### Käyttäjien poistaminen

Käyttäjiä voidaan poistaa kanavalta `/kick` komennolla.

`/kick` komennon jälkeen on annettava täsmällinen käyttäjänimimerkki, joka alkaa `@` merkillä. Esimerkiksi:

```
/kick @tuomo
```

Kun käyttäjä poistetaan kanavalta, asettuu hänelle automaattisesti kielto osallistua keskusteluun seuraavaan 30 minuuttiin. Hän kuitenkin näkee edelleen kanavalle tulevat viestit muilta käyttäjiltä, juuri kuten rekisteröitymättömät passiiviset käyttäjätkin näkevät.

### Flow-keskustelunhallintajärjestelmä

Kanavilla on käytössä dynaaminen keskustelunhallintajärjestelmä nimeltä _Flow_. _Flow_:t ovat risuaidalla varustettuja avainsanoja, kuten esimerkiksi **#verotiedot** tai **#sote-uudistus**. Flow:t ovat ikään kuin suodattimia, jotka suodattavat kaikki muut viestit pois viestinäkymästä paitsi valitun flow:n.

Flow-järjestelmän ajatus on mahdollistaa monen eri keskustelun käyminen samanaikaisesti kanavalla. Uusi _flow_ syntyy dynaamisesti silloin, kun käyttäjä lähettää uuden viestin uudella avainsanalla.

Esimerkiksi:

```
Hei mitä mieltä olette sote-uudistuksesta? #sote-uudistus
```

Tämä synnyttää kanavalle puheenaiheen (_flow_:n) nimeltä **#soteuudistus**. 

Flow:

* Jokainen viesti voi sisältää nollasta yhteen _flow_:ta. Toisin sanoen, yksi viesti voi kuulua maksimissaan yhteen keskustelunaiheesen.
* Voimassa olevat puheenaiheet eli _flow_:t näkyvät jatkuvasti kaikille käyttäjille _flow_-paneelissa, joka aukeaa painamalla risuaitaikonia tekstikentän vasemmalla puolella.
* Mobiililaitteella flow:ta pystyy myös selaamaan sivusuunnassa swaippaamalla.
* ALL-näkymässä näkyy kanavan kaikki viestit ilman mitään suodattimia.
* Flow:t erääntyvät kun niihin ei lähetetä uusia viestejä - tällä hetkellä yksinkertaisesti 30 minuutin jälkeen.
* Oranssilla huomiovärillä näkyvät flow-avainsanat ovat tällä hetkellä aktiivisa flow:ta. Nämä näkyvät myös yllä mainitussa flow-paneelissa.
* Harmaalla näkyvät flow:t viestien yhteydessä ovat kuolleita flow:ta. Nämä keskustelunaiheet eivät enää ole voimassa. Ne voidaan aloittaa uudelleen lähettämällä viesti samalla flow-avainsanalla.

Moderoijat pystyvät asettamaan tietyn _Flow_:n pysyväksi keskustelunaiheeksi komennolla:

```
/pin #flownimi
```

Tällöin kyseinen flow ei koskaan eräänny aktiivisten keskusteluaiheiden listalta.

_Flow_ palautetaan takaisin normaalisti erääntyväksi dynaamiseksi komennolla:

```
/unpin #flownimi
```

## 