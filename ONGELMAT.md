# Tiedossa olevat ongelmat

> Raportointi osoitteeseen comcomoy@gmail.com

Mobiili/Android:

* Viestinkirjoituskenttä hajoaa, kun viesti aloitetaan pisteellä. Tämä korruptoi Draft-JS viestieditorikirjaston tilan. Johtuu Facebookin kirjastolleen tarjoamasta puutteellisesta Android tuesta, ei helppoa korjausta odotettavissa pian. Tilapäinen korjaus työn alla.

Mobiili/iOS:

* Safari: Fokusointi tekstikentässä avaa näppäimistön. Tämä rikkoo mobiilinäkymän. Syy on se, että Safari-mobiiliselain ei tue `position: fixed` -tyylimääritelmää, jota yläpalkki sekä viestikenttä käyttävät. Korjauksessa. Lisätietoa: https://stackoverflow.com/questions/43833049/how-to-make-fixed-content-go-above-ios-keyboard. Tilapäinen ratkaisu: piilotetaan alusta iOS-laitteilta.
* Chrome: Näppäimistön avaus rikkoo mobiilinäkymän. Ei säädä kokoja uudelleen oikein. Korjauksessa. Tilapäinen ratkaisu: piilotetaan alusta iOS-laitteilta.

Safari:

* emoji-paneeli ei renderöidy vastaavalla tavalla kuin muilla selaimilla. Työn alla. Tilapäinen ratkaisu: piilotetaan alusta Safari-selaimelta.

Ominaisuuksien puute:

* **Toisen käyttäjän käyttäjäprofiilin tarkastelu**: työn alla.
* **Kanavanhallintapaneeli**: työn alla.
