# Tekninen ohjeistus

> Uutisjulkaisuja varten

Comcom on uutisjulkaisuja varten tuotettu reaaliaikainen keskustelualusta. Ajatuksena on, että _voit keskustella muiden kanssa, jotka ovat samaan aikaan samalla verkkosivulla kanssasi._

Verkkosivun ylläpidon näkökulmasta Comcom toimii teknisesti pitkälti vastaavalla tavalla kuin erinäiset asiakaspalveluchat-alustat, kuten _Intercom_. Ajatuksena on, että keskustelualusta lisätään jokaiselle sivuston sivulle (esim. `http://bbc.co.uk/europe/article.html`), ja alusta yhdistää jokaisella alasivullakin käyttäjän aina samaan päähuoneeseen `bbc.co.uk`.

Käytännössä Comcom-keskustelualusta toimii seuraavanlaisesti:

1. Uutisjulkaisun HTML-sivuun laitetaan `<body>` osion loppuun meidän oma `<script src="...">` -elementti
2. Kun lukija avaa verkkosivun, lataa lisätty skripti isäntäsivuston latautumisen jälkeen lopuksi meidän CDN:n kautta sovelluksemme Javascript-tiedoston (bundlen)
3. Kun sovellus on saanut avattua Websocket-yhteyden palvelimeemme, ilmestyy oikeaan alakulmaan sininen Comcom-ikoni
4. Lukija voi seurata reaaliaikaista keskustelua sivustolla ikonista avattavan komponentin kautta

Kun lukija haluaa osallistua keskusteluun pelkän seuraamisen sijaan hän:

1. Rekisteröityy sovelluksen kautta (Facebook-, Google- tai sähköpostirekisteröityminen)
2. Palvelu tallentaa käyttäjän kekseihin (cookies) oman domainimme alle (_comcom.chat_) käyttäjän tilitiedot
3. Nyt käyttäjä voi keskustella jokaisella sivulla omalla tunnuksellaan automaattisesti, missä Comcom on käytössä

Muutamia teknisiä yksityiskohtia:

- Skriptimme luo sivuston dokumenttioliomalliin (DOM) uuden `<div id="__COMCOM__ />` elemetin `<body>` viimeiseksi elementiksi. Sovellus renderöidään kokonaisuudessaan tähän elementtiin, eikä muuta mitään muuta elementtiä sivustolla
- Yllä oleva `__COMCOM__` elementti on muiden elementtien päällä yllä (korkea `z-index`-arvo) ja pysyy kiinteässä paikassa käyttäjän selaimessa (`position: fixed`)
- Sovellus ylläpitää websocket-yhteyttä palvelimeemme, jonka kautta kaikki viestit välittyvät palvelussa
- Jos sovellus ei onnistu avaamaan yhteyttä palvelimeemme, sovellus ei käynnisty ja ikoni ei tule näkyviin oikeaan alakulmaan. Tämä ei vaikuta isäntäsivuston toimintaan
- Sovelluksen kaikki tyylit (CSS) on rajattu `id="__COMCOM__` alle, eikä niiden pitäisi vaikuttaa isäntäsivustoon millään tavalla

Yllä oleva versio on keskeneräinen ja sovelluksen käyttöliittymäilme on menossa vielä kokonaan uusiksi joten visuaalisiin virheisiin ei kannata kiinnittää huomiota.
Yllä olevan demon tarkoitus on antaa sivuston ylläpitäjälle yleiskuva palvelun toimintaperiaatteesta sivustolla.

Kriittiset virheet ilmoitetaan osoitteeseen (ei vielä toiminnassa):
urgent@comcom.chat

Sovelluksen toimintaperiaatteesta antaa lisätietoja:
tuomo.hopia@gmail.com
